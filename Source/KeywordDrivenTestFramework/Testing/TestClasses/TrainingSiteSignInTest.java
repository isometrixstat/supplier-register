/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteSignInPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "SignIn into TrainingSite",
    createNewBrowserInstance = true
)
public class TrainingSiteSignInTest extends BaseClass
{

    String error = "";

    public TrainingSiteSignInTest()
    {

    }

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the Training Site URL
        if (!NavigateToIsometrixUrl())
        {
            return narrator.testFailed("Failed to navigate to Training Site signin page" + error);
        }

        // This step will sign into the specified Training Site account with the provided credentials
        if (!signInIntoTrainingSite())
        {
            return narrator.testFailed("Failed to navigate to Training Site signin page");
        }

        return narrator.finalizeTest("Successfully Navigated through Training Site signin page");
    }

    public boolean NavigateToIsometrixUrl()
    {

        if (!SeleniumDriverInstance.navigateTo(TrainingSiteSignInPageObjects.url()))
        {
            error = "Failed to navigate to Training Site";
            return false;
        }

        return true;
    }

    public boolean signInIntoTrainingSite()
    {

        if (!SeleniumDriverInstance.enterTextByXpath(TrainingSiteSignInPageObjects.userNameTextXpath(), testData.getData("Username")))
        {
            error = "Failed to enter text into username text field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(TrainingSiteSignInPageObjects.passwordTextXpath(), testData.getData("Password")))
        {
            error = "Failed to enter text into password text field.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteSignInPageObjects.signInButtontXpath()))
        {
            error = "Failed to click signin button.";
            return false;
        }
        
        pause(3000);
         if(!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteSignInPageObjects.isoSideMenu())){
            error = "Failed to wait for Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteSignInPageObjects.isoSideMenu())){
            error = "Failed to click Side Menu.";
            return false;
        }
        
        String version = SeleniumDriverInstance.retrieveTextByXpath(TrainingSiteSignInPageObjects.version());
        
        narrator.stepPassedWithScreenShot("Successfully extracted IsoMetrix Version: " + version + ".");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteSignInPageObjects.isoSideMenu())){
            error = "Failed to wait for Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteSignInPageObjects.isoSideMenu())){
            error = "Failed to click Side Menu.";
            return false;
        }
        return true;

    }

}
