/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author vijaya
 */
public class TrainingSiteSignInPageObjects extends BaseClass
{
 
    public static String url()
    {
        return "https://usazu-pr01.isometrix.net/IsoMetrix.UK.V4.Automation.1/default.aspx";
    }
       
    public static String userNameTextXpath()
    {
        return ".//input[@id='txtUsername']";
    }
    
    public static String passwordTextXpath()
    {
        return ".//input[@id='txtPassword']";
    }
    
    public static String signInButtontXpath()
    {
        return ".//div[@id='btnLoginSubmit']";
    }
    
    public static String isoSideMenu(){
        return "//i[@class='large sidebar link icon menu']";
    }
    
    public static String version(){
        return "//footer";
    }
}
